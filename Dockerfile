FROM openjdk:8-jre-alpine
Volume /tmp
ADD /target/*.jar petclinic.jar
EXPOSE 8080
CMD ["/usr/bin/java", "-jar", "petclinic.jar"]